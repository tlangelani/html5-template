<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="bowerlibs/bootstrap/dist/css/bootstrap.css"/>
    <link rel="stylesheet" href="css/style.css"/>
    <title>HTML5</title>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <h1>HTML5 Template</h1>
        </div>
        <div class="col-md-6">
            <h3>Chart Js</h3>
            <canvas id="chart" width="400" height="400"></canvas>
        </div>
    </div>
</div><!-- end .container -->

<script src="bowerlibs/jquery/dist/jquery.js"></script>
<script src="bowerlibs/bootstrap/dist/js/bootstrap.js"></script>
<script src="bowerlibs/Chart.js/Chart.js"></script>
<script src="js/script.js"></script>
</body>
</html>